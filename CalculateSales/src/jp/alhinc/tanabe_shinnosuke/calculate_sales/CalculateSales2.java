package jp.alhinc.tanabe_shinnosuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


class CalculateSales2 {

	//出力メソッド

	public static boolean output(HashMap<String, String> bNameMap, HashMap<String, Long> bSalesMap, String place, String name) {
		File branchOut = new File(place, name);
		BufferedWriter bw = null;
		try {
			FileWriter fw = new FileWriter(branchOut);
			bw = new BufferedWriter(fw);  //出力
				for(Map.Entry<String, String> entry : bNameMap.entrySet())	{
					bw.write(entry.getKey() + "," + entry.getValue() + "," + bSalesMap.get(entry.getKey()));
					bw.newLine();
				}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try	{
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//支店定義ファイル読み込みメソッド

	public static boolean input(HashMap<String, String> bNameMap, HashMap<String,Long> bSalesMap, String place, String name) {
		BufferedReader br = null; //tryでもfinallyでもbrが使えるようにする。
		try {
			File file = new File(place, name); //argsはファイル場所、branch.lstはファイル名
				if(!file.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return false;
				}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String branch;
			long N =0L;

			while((branch = br.readLine()) != null) {			//nullでなければ、1行を読み込む
				String[] nb = branch.split(",");
					if(nb.length == 2 && nb[0].matches("\\d{3}")) {
						bNameMap.put(nb[0],  nb[1]);//whileで1行ずつ読むのでsplitした前者(コード)と後者(支店名)を格納
						bSalesMap.put(nb[0], N);
					} else {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return false;
					}
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally {									   //finallyがないとbrのcloseが正常に行えない。
			if(br != null) {
				try	{
				   br.close();
				}  catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	//集計メソッド

	public static boolean calc(HashMap<String, Long> bSalesMap, String place) {
		File dir = new File(place);

		//ファイルの一覧取得

		File[] dirFiles = dir.listFiles();
		ArrayList<File> rcdFiles = new ArrayList<File>();
		for(int i = 0; i < dirFiles.length; i++) {
			if(dirFiles[i].getName().matches("\\d{8}.rcd$") && dirFiles[i].isFile()) {
				rcdFiles.add(dirFiles[i]);
			}
		}

		//拡張子削除

		ArrayList<String> fileNames = new ArrayList<String>();
		for(int i = 0; i < rcdFiles.size(); i++) {
			File f = rcdFiles.get(i);
			String file = f.getName();
			String fName = file.substring(0, file.lastIndexOf("."));
			fileNames.add(fName);
		}

		//連番チェックループ
		for(int i = 1; i < fileNames.size(); i++) {
			int x = Integer.parseInt(fileNames.get(i));
			int y = Integer.parseInt(fileNames.get(i - 1));
			int z = x - y;
			if(z != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}


		//ArrayListのファイル読み込み

		for(int i = 0; i < rcdFiles.size(); i++)  {
			ArrayList<String> codeSales = new ArrayList<String>();
			BufferedReader brs = null;
			try {
				FileReader frs = new FileReader(rcdFiles.get(i));
				brs = new BufferedReader(frs);
				String sales;
				while((sales = brs.readLine()) != null) {
					codeSales.add(sales); //1ループで要素2のlist作成
				}



				if(codeSales.size() !=2 ) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return false;
				}

				if(bSalesMap.get(codeSales.get(0)) == null) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return false;
				}


			} catch(IOException e) {
				System.out.println(e);
				return false;
			} finally {
				if(brs != null) {
					try {
						brs.close();
						} catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return false;
						}
				}
			}

			if(codeSales.get(1).matches("^[0-9]*$")) {
				long L =Long. parseLong(codeSales.get(1)); //売り上げ額をlong型へ
				long totalSales = bSalesMap.get(codeSales.get(0)) + L;

			//10桁以上の処理

				if(totalSales > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				} else {
					bSalesMap.put(codeSales.get(0), totalSales);
				}
			} else {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}


	public static void main(String[] args) {
		HashMap<String, String> branchNameMap = new HashMap<String, String>();
		HashMap<String, Long> branchSalesMap = new HashMap<String, Long>();
		if(args.length !=1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//支店定義ファイル読み込みメソッド

		if(input(branchNameMap, branchSalesMap, args[0], "branch.lst") == false) {
			return;
		}

		//集計メソッド呼び出し

		if(calc(branchSalesMap, args[0]) == false) {
			return;
		}

		//出力メソッドの呼び出し

		if(output(branchNameMap, branchSalesMap, args[0], "branch.out") == false) {
			return;
		}
	}
}
